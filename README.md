## Intro

My collection of user scripts for Chrome's Tampermonkey.

## Playback rate

### Description

Configure audio and video player's playback rate via global `$s` 
(think as `speed`) variable.

Works on web-sites like: https://soundcloud.com, https://vimeo.com etc.

### Usage

Change global `$s` variable. Something like `$s = 1.5` and then play any html5 
player.

Or even start to play any player and then change variable `$s = 2`, `$s = 1.5` 
etc. 
Until you satisfied with playback. 

It will take effect immediately.

### Source

[playbackRate.js](playbackRate.js)
