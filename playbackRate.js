// ==UserScript==
// @name         Playback rate
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Configure HTML playback rate
// @author       You
// @match        *://*/*
// @grant        none
// @noframes
// ==/UserScript==

(function () {
    var speedValue = 1;
    var currentPlayer = {paused: true};
    var inIfrmame = window !== window.top;

    var play = {
        audio: HTMLAudioElement.prototype.play,
        video: HTMLVideoElement.prototype.play,
    };

    function patch(type) {
        return function () {
            currentPlayer = this;
            this.playbackRate = window.$s || 1;
            return play[type].apply(this, arguments);
        };
    }

    function createGlobalVariable() {
        Object.defineProperty(window, '$s', {
            enumerable: true,
            configurable: true,
            get: function () {return speedValue;},
            set: function (newSpeed) {
                speedValue = newSpeed;

                if (!currentPlayer.paused) {
                    currentPlayer.pause();
                    currentPlayer.play();
                }
            }
        });
    }

    function throwIfOccupied() {
        if (window.$s) {
            setTimeout(function () {
                throw new Error('This site is already using window.$s variable.');
            }, 0);
        }
    }

    function init() {
        throwIfOccupied();
        createGlobalVariable();
        HTMLAudioElement.prototype.play = patch('audio');
        HTMLVideoElement.prototype.play = patch('video');
    }

    if (!inIfrmame) {
        init();
    }
}());
